namespace diaexpert.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class diacontext : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.patients", "aadhar", c => c.String());
            AddColumn("dbo.patients", "photo", c => c.String());
            AddColumn("dbo.patients", "diabetichistory", c => c.String());
            AddColumn("dbo.patients", "currentmedications", c => c.String());
            AddColumn("dbo.patients", "surgicalhistory", c => c.String());
            AddColumn("dbo.patients", "allergies", c => c.String());
            AddColumn("dbo.patients", "lastvisit", c => c.DateTime());
            AddColumn("dbo.patients", "medialreports", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.patients", "medialreports");
            DropColumn("dbo.patients", "lastvisit");
            DropColumn("dbo.patients", "allergies");
            DropColumn("dbo.patients", "surgicalhistory");
            DropColumn("dbo.patients", "currentmedications");
            DropColumn("dbo.patients", "diabetichistory");
            DropColumn("dbo.patients", "photo");
            DropColumn("dbo.patients", "aadhar");
        }
    }
}
