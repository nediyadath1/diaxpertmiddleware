namespace diaexpert.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.doctors",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        mobile = c.String(),
                        address = c.String(),
                        credentials = c.String(),
                        level = c.Int(nullable: false),
                        photo = c.String(),
                        pincode = c.String(),
                        lat = c.Single(nullable: false),
                        lan = c.Single(nullable: false),
                        timestampe = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.patients",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        mobile = c.String(),
                        age = c.Byte(nullable: false),
                        name = c.String(),
                        address = c.String(),
                        pin = c.String(),
                        lat = c.Single(nullable: false),
                        lan = c.Single(nullable: false),
                        comorbidities = c.String(),
                        timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.reports",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        docid = c.Int(nullable: false),
                        patientid = c.Int(nullable: false),
                        consulttime = c.DateTime(nullable: false),
                        consultreport = c.String(),
                        prescription = c.String(),
                        timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.doctors", t => t.docid, cascadeDelete: true)
                .ForeignKey("dbo.patients", t => t.patientid, cascadeDelete: true)
                .Index(t => t.docid)
                .Index(t => t.patientid);
            
            CreateTable(
                "dbo.schedules",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        docid = c.Int(nullable: false),
                        patientid = c.Int(nullable: false),
                        consultdatetime = c.DateTime(nullable: false),
                        timestamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.doctors", t => t.docid, cascadeDelete: true)
                .ForeignKey("dbo.patients", t => t.patientid, cascadeDelete: true)
                .Index(t => t.docid)
                .Index(t => t.patientid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.schedules", "patientid", "dbo.patients");
            DropForeignKey("dbo.schedules", "docid", "dbo.doctors");
            DropForeignKey("dbo.reports", "patientid", "dbo.patients");
            DropForeignKey("dbo.reports", "docid", "dbo.doctors");
            DropIndex("dbo.schedules", new[] { "patientid" });
            DropIndex("dbo.schedules", new[] { "docid" });
            DropIndex("dbo.reports", new[] { "patientid" });
            DropIndex("dbo.reports", new[] { "docid" });
            DropTable("dbo.schedules");
            DropTable("dbo.reports");
            DropTable("dbo.patients");
            DropTable("dbo.doctors");
        }
    }
}
