﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(diaexpert.Startup))]
namespace diaexpert
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
