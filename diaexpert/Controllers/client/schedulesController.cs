﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using diaexpert.Models;

namespace diaexpert.Controllers.client
{
    [RoutePrefix("schedule")]
    public class schedulesController : ApiController
    {
        private diacontext db = new diacontext();

        // GET: api/schedules
        //public IQueryable<schedule> Getsch()
        //{
        //    return db.sch;
        //}
        [ResponseType(typeof(List<docschedule>))]
        [Route("GetFreeSlots")]
        public IHttpActionResult GetFreeSlots(int id)
        {
            DateTime sdate = DateTime.Now;
            int dayscount = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            DateTime edate = DateTime.Now.AddDays(dayscount-1);
            List<schedule> docschedule = db.sch.Where(e => e.docid == id && e.consultdatetime >= sdate && e.consultdatetime <= edate).ToList();
            List<docschedule> dsch = new List<docschedule>();
            for (int i = 1; i <= dayscount; i++)
            {
                docschedule d = new docschedule();
                DateTime dt;
                int count = docschedule.Where(e => e.consultdatetime.Day == i).Count();
                try
                {
                   dt = docschedule.FirstOrDefault(f => f.consultdatetime.Day == i).consultdatetime;
                }
                catch
                {
                    string datetime = i < DateTime.Now.Day ? DateTime.Now.AddMonths(1).Year + "-" + DateTime.Now.AddMonths(1).Month + "-" + i: DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + i;
              dt=Convert.ToDateTime(datetime);
                }
                    if (count < 20)
                {
                    d.consultDate = dt;
                    d.scheduledConsults = count;
                    dsch.Add(d);
                }
            }
            return Ok(dsch);
        }

        // GET: api/schedules/5
        [ResponseType(typeof(schedule))]
        public IHttpActionResult Getschedule(int id)
        {
            schedule schedule = db.sch.Find(id);
            if (schedule == null)
            {
                return NotFound();
            }

            return Ok(schedule);
        }

        // PUT: api/schedules/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putschedule(int id, schedule schedule)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != schedule.id)
            {
                return BadRequest();
            }

            db.Entry(schedule).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!scheduleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/schedules
        [ResponseType(typeof(schedule))]
        public IHttpActionResult Postschedule(schedule schedule)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.sch.Add(schedule);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = schedule.id }, schedule);
        }

        // DELETE: api/schedules/5
        [ResponseType(typeof(schedule))]
        public IHttpActionResult Deleteschedule(int id)
        {
            schedule schedule = db.sch.Find(id);
            if (schedule == null)
            {
                return NotFound();
            }

            db.sch.Remove(schedule);
            db.SaveChanges();

            return Ok(schedule);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool scheduleExists(int id)
        {
            return db.sch.Count(e => e.id == id) > 0;
        }
    }
}