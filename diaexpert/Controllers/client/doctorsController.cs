﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Xml.Schema;
using diaexpert.Models;

namespace diaexpert.Controllers.client
{
    public class doctorsController : ApiController
    {
        private diacontext db = new diacontext();

        // GET: api/doctors
        public IQueryable<doctor> Getdoc()
        {
            return db.doc;
        }

        // GET: api/doctors/5
        [ResponseType(typeof(doctor))]
        public IHttpActionResult Getdoctor(int id)
        {
            doctor doctor = db.doc.Find(id);
            if (doctor == null)
            {
                return NotFound();
            }

            return Ok(doctor);
        }

        // PUT: api/doctors/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult Putdoctor(int id, doctor doctor)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

            //    if (id != doctor.id)
            //    {
            //        return BadRequest();
            //    }

            //    db.Entry(doctor).State = EntityState.Modified;

            //    try
            //    {
            //        db.SaveChanges();
            //    }
            //    catch (DbUpdateConcurrencyException)
            //    {
            //        if (!doctorExists(id))
            //        {
            //            return NotFound();
            //        }
            //        else
            //        {
            //            throw;
            //        }
            //    }

            //    return StatusCode(HttpStatusCode.NoContent);
            //}

            //// POST: api/doctors
            //[ResponseType(typeof(doctor))]
            //public IHttpActionResult Postdoctor(doctor doctor)
            //{
            //    if (!ModelState.IsValid)
            //    {
            //        return BadRequest(ModelState);
            //    }

            //    db.doc.Add(doctor);
            //    db.SaveChanges();

            //    return CreatedAtRoute("DefaultApi", new { id = doctor.id }, doctor);
            //}

            //// DELETE: api/doctors/5
            //[ResponseType(typeof(doctor))]
            //public IHttpActionResult Deletedoctor(int id)
            //{
            //    doctor doctor = db.doc.Find(id);
            //    if (doctor == null)
            //    {
            //        return NotFound();
            //    }

            //    db.doc.Remove(doctor);
            //    db.SaveChanges();

            //    return Ok(doctor);
            //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doctorExists(int id)
        {
            return db.doc.Count(e => e.id == id) > 0;
        }
    }
}