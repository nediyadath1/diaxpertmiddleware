﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using diaexpert.Models;

namespace diaexpert.Controllers.client
{
    public class patientsController : ApiController
    {
        private diacontext db = new diacontext();

        // GET: api/patients
        //public IQueryable<patient> Getpt()
        //{
        //    return db.pt;
        //}

        // GET: api/patients/5
        [ResponseType(typeof(patient))]
        public IHttpActionResult Getpatient(int id)
        {
            patient patient = db.pt.Find(id);
            if (patient == null)
            {
                return NotFound();
            }

            return Ok(patient);
        }

        // PUT: api/patients/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putpatient(int id, patient patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != patient.id)
            {
                return BadRequest();
            }

            db.Entry(patient).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!patientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/patients
        [ResponseType(typeof(patient))]
        public IHttpActionResult Postpatient(patient patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.pt.Add(patient);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = patient.id }, patient);
        }

        // DELETE: api/patients/5
        [ResponseType(typeof(patient))]
        //public IHttpActionResult Deletepatient(int id)
        //{
        //    patient patient = db.pt.Find(id);
        //    if (patient == null)
        //    {
        //        return NotFound();
        //    }

        //    db.pt.Remove(patient);
        //    db.SaveChanges();

        //    return Ok(patient);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool patientExists(int id)
        {
            return db.pt.Count(e => e.id == id) > 0;
        }
    }
}