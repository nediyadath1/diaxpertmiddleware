﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using diaexpert.Models;

namespace diaexpert.Controllers.admin
{
    public class schedulesController : Controller
    {
        private diacontext db = new diacontext();

        // GET: schedules
        public async Task<ActionResult> Index()
        {
            var sch = db.sch.Include(s => s.doc).Include(s => s.pt);
            return View(await sch.ToListAsync());
        }

        // GET: schedules/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            schedule schedule = await db.sch.FindAsync(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // GET: schedules/Create
        public ActionResult Create()
        {
            ViewBag.docid = new SelectList(db.doc, "id", "name");
            ViewBag.patientid = new SelectList(db.pt, "id", "mobile");
            return View();
        }

        // POST: schedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,docid,patientid,consultdatetime,timestamp")] schedule schedule)
        {
            if (ModelState.IsValid)
            {
                db.sch.Add(schedule);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.docid = new SelectList(db.doc, "id", "name", schedule.docid);
            ViewBag.patientid = new SelectList(db.pt, "id", "mobile", schedule.patientid);
            return View(schedule);
        }

        // GET: schedules/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            schedule schedule = await db.sch.FindAsync(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.docid = new SelectList(db.doc, "id", "name", schedule.docid);
            ViewBag.patientid = new SelectList(db.pt, "id", "mobile", schedule.patientid);
            return View(schedule);
        }

        // POST: schedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,docid,patientid,consultdatetime,timestamp")] schedule schedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(schedule).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.docid = new SelectList(db.doc, "id", "name", schedule.docid);
            ViewBag.patientid = new SelectList(db.pt, "id", "mobile", schedule.patientid);
            return View(schedule);
        }

        // GET: schedules/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            schedule schedule = await db.sch.FindAsync(id);
            if (schedule == null)
            {
                return HttpNotFound();
            }
            return View(schedule);
        }

        // POST: schedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            schedule schedule = await db.sch.FindAsync(id);
            db.sch.Remove(schedule);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
