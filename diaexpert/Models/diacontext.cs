﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace diaexpert.Models
{
    public class diacontext:DbContext
    {
        public DbSet<doctor> doc { get; set; }
        public DbSet<patient> pt { get; set; }
        public DbSet<report> rpt { get; set; }
        public DbSet<schedule> sch { get; set; }
    }
}