﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services.Description;

namespace diaexpert.Models
{
    public class doctor
    {
        public int id { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string address { get; set; }
        public string credentials { get; set; }
        public level level { get; set; }
        public string photo { get; set; }
        public string pincode { get; set; }
        public float lat { get; set; }
        public float lan { get; set; }
        public DateTime timestampe { get; set; } = DateTime.Now;

    }

    public enum level
    {
       specialist,
       gp,
       paramedic,
       other
    }

    public class schedule {
        public int id { get; set; }
        [ForeignKey("doc")]
        public int docid { get; set; }
        public doctor doc { get; set; }
        [ForeignKey("pt")]
        public int patientid{ get; set; }
        public patient pt { get; set; }

        public DateTime consultdatetime { get; set; }
        public DateTime timestamp { get; set; } = DateTime.Now;

    }

    public class patient
    {
        public int id { get; set; }
        public string aadhar { get; set; }
        public string photo { get; set; }
        public string mobile { get; set; }
        public byte age { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string pin { get; set; }
        public float lat { get; set; }
        public float lan { get; set; }
        public string comorbidities { get; set; }
        public string diabetichistory { get; set; }
        public string currentmedications { get; set; }
        public string surgicalhistory { get; set; }
        public string allergies { get; set; }
        public DateTime? lastvisit { get; set; }
        public string medialreports { get; set; }
        public DateTime timestamp { get; set; } = DateTime.Now;

    }

    public class report
    {
        public int id { get; set; }
        [ForeignKey("doc")]
        public int docid { get; set; }
        public doctor doc { get; set; }
        [ForeignKey("pt")]
        public int patientid { get; set; }
        public patient pt { get; set; }
        public DateTime consulttime { get; set; }
        public string consultreport { get; set; }
        public string prescription { get; set; }
        public DateTime timestamp { get; set; }

    }
}